#pragma once

#include <ofMain.h>
#include "ofxOscRouter.h"

#include <deque>

#include "siBullet.h"
#include "siBarre.h"
#include "siMonstre.h"
#include "siVaisseau.h"


class ofApp : public ofBaseApp, public ofxOscRouter
    {
    public:
        ofApp();

		void setup();
		void update();
		void draw();

		void keyPressed(int key);
		void mouseMoved(int x, int y );
		void mousePressed(int x, int y, int button);
		void windowResized(int w, int h);

                void processOscCommand(const std::string& command, const ofxOscMessage& m);

        std::deque<siBullet> bullets;
        siBarre * ma_barre;
        siMonstre mon_monstre;
        siVaisseau mon_vaisseau;
};

