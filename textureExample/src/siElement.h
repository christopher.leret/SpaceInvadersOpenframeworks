#pragma once
#include <ofMain.h>

class siElement : public ofNode
{
 public:
  siElement();
  ~siElement();

  virtual void update() = 0;
  virtual void customDraw() = 0;
};
