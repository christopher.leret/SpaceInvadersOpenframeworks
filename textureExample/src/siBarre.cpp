#include "siBarre.h"

siBarre::siBarre() :
  siElement(),
  width(0),
  height(0)
{
  couleur = ofColor(ofColor::red);
  setPosition(0, 160, 0);
}

siBarre::~siBarre()
{

}

void siBarre::customDraw()
{
  ofPushMatrix();
  ofPushStyle();
  ofSetColor(couleur);
  ofDrawRectangle(getX(), getY(), width, 20);
  ofPopStyle();
  ofPopMatrix();
}

void siBarre::update()
{

}

void siBarre::mouseMoved(int x, int y)
{
  setPosition(0, y, 0);
}

void siBarre::windowResized(int w, int h)
{
	width = w;
	height = h;
}
