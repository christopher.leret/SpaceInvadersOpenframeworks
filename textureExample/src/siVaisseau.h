#include "siElement.h"
#include "ofxOscRouterNode.h"

class siVaisseau : public siElement, public ofxOscRouterNode
{
public :
    siVaisseau();
    ~siVaisseau();

    void setup();
    void update();
    void customDraw();
    void mouseMoved(int x,int y);
    void processOscCommand(const std::string& command, const ofxOscMessage& m);


    ofTexture 		texColor;

    char colonneVaisseaux, ligneVaisseaux ,wVaisseaux ,hVaisseaux, departVaisseaux;
    int nbDecalVaisseaux;

    enum TYPE_CELLULE {
        CELLULE_VIDE,
        CELLULE_DEGRADE,
        CELLULE_YEUX,
        CELLULE_VAISSEAUX
    };

    int tabVaisseaux [3][5] =
    {
    {CELLULE_VIDE,CELLULE_VIDE,CELLULE_VAISSEAUX,CELLULE_VIDE,CELLULE_VIDE},
    {CELLULE_VIDE,CELLULE_VAISSEAUX,CELLULE_VAISSEAUX,CELLULE_VAISSEAUX,CELLULE_VIDE},
    {CELLULE_VAISSEAUX,CELLULE_VAISSEAUX,CELLULE_VAISSEAUX,CELLULE_VAISSEAUX,CELLULE_VAISSEAUX}
    };

    ofPixels colorPixels;
};

