#include "siBullet.h"

siBullet::siBullet(int _x, int _y) :
    siElement(),
    width(5),
    height(10),
    should_die(false)
{
    couleur = ofColor(ofColor::red);
    setPosition(_x, _y, 0);
}

siBullet::siBullet()
{
    siBullet(0, 0);
}

siBullet::~siBullet()
{
}

void siBullet::customDraw()
{
    ofPushMatrix();
    ofPushStyle();
    ofSetColor(couleur);
    ofDrawRectangle(getX(), getY(), width, height);
    ofPopStyle();
    ofPopMatrix();
}

void siBullet::update()
{
    if(getY() > 0)
    {
        move(0, -1, 0);
    }
    else{
        should_die = true;
    }
}
