#include "siVaisseau.h"

siVaisseau::siVaisseau() :
  siElement(),
  ofxOscRouterNode("vaisseau")
{

}

siVaisseau::~siVaisseau()
{

}


void siVaisseau::setup()
{
    colonneVaisseaux = 5;
    ligneVaisseaux = 3;
    wVaisseaux = 25;
    hVaisseaux = 25;
    departVaisseaux = 0;


    colorPixels.allocate(wVaisseaux,hVaisseaux,OF_PIXELS_RGB);

    // color pixels, use x and y to control red and green
    for (int y = 0; y < hVaisseaux; y++){
        for (int x = 0; x < wVaisseaux; x++){
            colorPixels.setColor(x,y,ofColor(x,y,0));
        }
    }

    texColor.allocate(colorPixels);

}

void siVaisseau::update()
{
}

void siVaisseau::customDraw()
{
    for(int i=0;i<colonneVaisseaux;i++)
    {
        for(int j=0;j<ligneVaisseaux;j++)
        {
            if(tabVaisseaux[j][i]==CELLULE_VAISSEAUX)
            {
                if(j==0)
                {
                texColor.draw(getX()-75, ofGetHeight() - (hVaisseaux*3 + getY()), wVaisseaux, hVaisseaux);
                }
                if(j==1)
                {
                    i++;
                    departVaisseaux = 25*i;
                    texColor.draw(getX()-departVaisseaux,ofGetHeight() - (hVaisseaux*2 + getY()),wVaisseaux,hVaisseaux);
                    i--;
                }
                if(j==2)
                {
                    i++;
                    departVaisseaux = 25*i;
                    texColor.draw(getX()-departVaisseaux,ofGetHeight() - (hVaisseaux + getY()),wVaisseaux,hVaisseaux);
                    i--;
                }
            }
        }
    }
}

void siVaisseau::mouseMoved(int x, int y)
{
  setPosition(x, 100, 0);
}

//--------------------------------------------------------------
void siVaisseau::processOscCommand(const std::string& command, const ofxOscMessage& m){
    if(command == "position")
        setPosition(getArgAsIntUnchecked(m, 0), getArgAsIntUnchecked(m, 1), 0);
}
