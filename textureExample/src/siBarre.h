#pragma once
#include "siElement.h"

class siBarre : public siElement{
 public:
  siBarre();
  ~siBarre();

  void customDraw();
  void update();
  void mouseMoved(int x, int y);
  void windowResized(int w, int h);

  ofColor couleur;
  int width;
  int height;
};
