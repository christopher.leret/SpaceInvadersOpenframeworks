#include "siMonstre.h"

siMonstre::siMonstre() :
  siElement()
{

}

siMonstre::~siMonstre()
{

}

void siMonstre::setup()
{
    compteur = 0;
    ligne = 8;
    colonne = 11;
    mouvementHorizontaux = true;
    nbDecal = 0;
    descente=0;
    wMonstre = 25;
    hMonstre = 25;

    grayPixels.allocate(wMonstre,hMonstre,OF_PIXELS_RGB);
    colorPixels.allocate(wMonstre,hMonstre,OF_PIXELS_RGB);

    // gray pixels, set them randomly
    for (int i = 0; i < wMonstre*hMonstre; i++){
        grayPixels[i] = 0;

    }

    // color pixels, use x and y to control red and green
    for (int y = 0; y < hMonstre; y++){
        for (int x = 0; x < wMonstre; x++){
            colorPixels.setColor(x,y,ofColor(x,y,0));
        }
    }

    texGray.allocate(grayPixels);
    texColor.allocate(colorPixels);

}

void siMonstre::update()
{
    if(mouvementHorizontaux==false)
    {
        if(descente>=200)
        {
            nbDecal+=10;
        }
        else
        {
            nbDecal+=2;
        }
        if(nbDecal>=1000)
        {
            mouvementHorizontaux=true;
            descente+=25;
        }
    }

    if(mouvementHorizontaux==true)
    {
        if(descente>=200)
        {
            nbDecal-=10;
        }
        else
        {
            nbDecal-=2;
        }
        if(nbDecal<=0)
        {
            mouvementHorizontaux=false;
            descente+=25;
        }
    }
}

void siMonstre::customDraw()
{
    //YEUX ALIEN
    compteur++;
    if(compteur > 5760){
       compteur = 0;
    }

    unsigned char val = ((cos(0.0625*compteur)*255+255)/2);
    for (int i = 0; i < wMonstre*hMonstre; i++){
        grayPixels[i] = val;
    }


    //ALIEN
    texGray.loadData(grayPixels.getData(), wMonstre,hMonstre, GL_LUMINANCE);

    ofSetHexColor(0xffffff);

    for(int i=0;i<colonne;i++)
    {
        for(int j=0;j<ligne;j++)
        {
            if(tab[j][i]==CELLULE_YEUX)
            {
                texGray.draw(i*wMonstre+nbDecal,j*hMonstre+descente,wMonstre,hMonstre);
            }
            if(tab[j][i]==CELLULE_DEGRADE)
            {
                texColor.draw(i*wMonstre+nbDecal,j*hMonstre+descente,wMonstre,hMonstre);
            }
        }
    }

}

void siMonstre::mousePressed(int x, int y, int button)
{
    int moussePressedX = x/25-(nbDecal/25);
    int moussePressedY = y/25-(descente/25);


    if(tab[moussePressedY][moussePressedX]==CELLULE_DEGRADE)
    {
        tab[moussePressedY][moussePressedX]=CELLULE_YEUX;
    }
}
