#pragma once
#include "siElement.h"

class siBullet : public siElement
{
public:
    siBullet(int _x, int _y);
    siBullet();
    ~siBullet();

    void customDraw();
    void update();

    ofColor couleur;
    int width;
    int height;
    bool should_die;
};
