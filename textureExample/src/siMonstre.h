#include "siElement.h"

class siMonstre : public siElement{
public :
    siMonstre();
    ~siMonstre();

    void setup();
    void update();
    void customDraw();
    void mousePressed(int x, int y, int button);

    ofTexture		texGray;
    ofTexture 		texColor;

    int compteur,wMonstre,hMonstre,ligne,colonne,nbDecal,descente;
    bool mouvementHorizontaux;

    enum TYPE_CELLULE {
        CELLULE_VIDE,
        CELLULE_DEGRADE,
        CELLULE_YEUX,
    };

    int tab[8][11]=
    {
        {CELLULE_VIDE,CELLULE_VIDE,CELLULE_DEGRADE,CELLULE_VIDE,CELLULE_VIDE,CELLULE_VIDE,CELLULE_VIDE,CELLULE_VIDE,CELLULE_DEGRADE,CELLULE_VIDE,CELLULE_VIDE},
        {CELLULE_VIDE,CELLULE_VIDE,CELLULE_VIDE,CELLULE_DEGRADE,CELLULE_VIDE,CELLULE_VIDE,CELLULE_VIDE,CELLULE_DEGRADE,CELLULE_VIDE,CELLULE_VIDE,CELLULE_VIDE},
        {CELLULE_VIDE,CELLULE_VIDE,CELLULE_DEGRADE,CELLULE_DEGRADE,CELLULE_DEGRADE,CELLULE_DEGRADE,CELLULE_DEGRADE,CELLULE_DEGRADE,CELLULE_DEGRADE,CELLULE_VIDE,CELLULE_VIDE},
        {CELLULE_VIDE,CELLULE_DEGRADE,CELLULE_DEGRADE,CELLULE_YEUX,CELLULE_DEGRADE,CELLULE_DEGRADE,CELLULE_DEGRADE,CELLULE_YEUX,CELLULE_DEGRADE,CELLULE_DEGRADE,CELLULE_VIDE},
        {CELLULE_DEGRADE,CELLULE_DEGRADE,CELLULE_DEGRADE,CELLULE_DEGRADE,CELLULE_DEGRADE,CELLULE_DEGRADE,CELLULE_DEGRADE,CELLULE_DEGRADE,CELLULE_DEGRADE,CELLULE_DEGRADE,CELLULE_DEGRADE},
        {CELLULE_DEGRADE,CELLULE_VIDE,CELLULE_DEGRADE,CELLULE_DEGRADE,CELLULE_DEGRADE,CELLULE_DEGRADE,CELLULE_DEGRADE,CELLULE_DEGRADE,CELLULE_DEGRADE,CELLULE_VIDE,CELLULE_DEGRADE},
        {CELLULE_DEGRADE,CELLULE_VIDE,CELLULE_DEGRADE,CELLULE_VIDE,CELLULE_VIDE,CELLULE_VIDE,CELLULE_VIDE,CELLULE_VIDE,CELLULE_DEGRADE,CELLULE_VIDE,CELLULE_DEGRADE},
        {CELLULE_VIDE,CELLULE_VIDE,CELLULE_VIDE,CELLULE_DEGRADE,CELLULE_DEGRADE,CELLULE_VIDE,CELLULE_DEGRADE,CELLULE_DEGRADE,CELLULE_VIDE,CELLULE_VIDE,CELLULE_VIDE}
    };

    ofPixels colorPixels;
    ofPixels grayPixels;

};
