#include "ofApp.h"

//--------------------------------------------------------------
ofApp::ofApp() :
    ofBaseApp(),
    ofxOscRouter("spaceI", 1234)
{
    ofxOscRouterNode::addOscMethod("exit");
}

//--------------------------------------------------------------
void ofApp::setup(){
    ofSetFrameRate(60);

    ma_barre = new siBarre();
    mon_monstre = siMonstre();
    mon_monstre.setup();
    mon_vaisseau.setup();

    ofxOscRouterNode::addOscChild(&mon_vaisseau);
}


//--------------------------------------------------------------
void ofApp::update(){   
    ofxOscRouter::update();
    ma_barre->update();
    mon_monstre.update();
    if(!bullets.empty()){
        for(std::deque<siBullet>::iterator it=bullets.begin(); it != bullets.end(); ++it){
            (*it).update();
        }
        if((*bullets.begin()).should_die)
        {
            bullets.pop_front();
        }
    }
}

//--------------------------------------------------------------
void ofApp::draw(){

    ofColor colorOne(47,61,131);
    ofColor colorTwo(147,165,254);

    ofBackgroundGradient(colorOne, colorTwo, OF_GRADIENT_LINEAR);
    ma_barre->customDraw();
    mon_monstre.customDraw();
    mon_vaisseau.customDraw();
    if(!bullets.empty()){
        for(std::deque<siBullet>::iterator it=bullets.begin(); it != bullets.end(); ++it){
            (*it).customDraw();
        }
    }
    ofDrawBitmapString("Nombre de bullets " + ofToString(bullets.size()), 100, 100);
}

//--------------------------------------------------------------
void ofApp::keyPressed  (int key){

	if(key == ' ')
		bullets.push_back(siBullet(300, 300));
}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){


    if(x<125)
    {
        x=125;
    }
    mon_vaisseau.mouseMoved(x,y);
    ma_barre->mouseMoved(x, y);
}


//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){
    mon_monstre.mousePressed(x,y,button);
}

//--------------------------------------------------------------
void ofApp::processOscCommand(const std::string& command, const ofxOscMessage& m){
    ofLogError("SI", "Commande OSC reçue : " + command + " de la part de " + m.getRemoteIp());
}

void ofApp::windowResized(int w, int h){
    ma_barre->windowResized(w, h);
}
